# MAODV---Energy-Aware-Path

Topik Khusus B

Nama Kelompok 
1. Farhan Zuhdi           05111640000070
2. Mohammad Haekal Alawy  05111640000141
3. Naufal Andira Perdana  05111640000150
4. Farras Rabbani         05111640000155

# 1. Konsep 


## 1.1 Deskripsi Paper 

Implementasi MAODV didasarkan pada paper berikut :

Judul : Energy Aware Path Selection based Efficient AODV for MANETs<br/>
Penulis : Muhammad Khalid Riaz, Fan Yangyu, Imran Akhtar<br/>
Sumber : https://www.researchgate.net/publication/331941804_Energy_Aware_Path_Selection_based_Efficient_AODV_for_MANETs<br/>

## 1.2 Latar Belakang 

Pada Mobile Adhoc Network selama proses routing mengkonsumsi energi, dimana itu  menjadi sebuah tantangan karena mobile nodes memiliki baterai yang terbatas. Jadi diusulkan dua metode untuk meningkatkan masa pakai jaringan dan mengurangi kerusakan koneksi dengan memilih jalur routing dengan lebih banyak energi yang tersedia.

## 1.3 Dasar Teori 


### 1.3.1 Ad Hoc On-demand Distance Vector Routing Protocol (AODV Routing Protocol) 

AODV adalah routing protocol yang termasuk kategori reactive routing protocol. Seperti reactive routing protocol lainnya, AODV hanya menyimpan informasi routing seputar path dan host yang aktif. Didalam AODV, informasi routing disimpan di semua node. Setiap node menyimpan tabel routing next-hop, dimana menyimpan informasi tujuan ke hop berikutnya dimana node tersebut memiliki route tertentu. Didalam AODV, ketika node asal ingin mengirim packet ke tujuan namun tidak ada route yang tersedia, node tersebut akan memulai proses route discovery. Dalam route discovery ada dua tahapan yang dilakukan, yakni:
* Route Request (RREQ), yakni ketika ada node sumber yang ingin mengirimkan paket ke node destination, dan node sumber tersebut masih belum memiliki rute untuk mengirim paket tersebut
* Route Reply, (RREP), yakni dibuat oleh node tujuan atau node yang mempunyai rute menuju tujuan	
* Route Error (RERR) 	

## 1.4 Solusi yang diusulkan
Estimasi zona area node dibagi dan membagi sinyal yang diterima dari pengirim menjadi 3 zona yang berbeda, yaitu: Inner zone, Middle zone dan Outer zone.<br/>
Dengan menentukan nilai treshold dari kekuatan sinyal yang diterima untuk membatasi node yang berada di inner dan outer zone dalam proses pencarian rute. Dan hanya menggunakan middle zone.<br/>
Saat node destinasi menerima RREQ, tidka akan langsung mengirimkan RREP, namun akan menunggu selama waktu T untuk RREQ lain. Yang kemudian akan dibandingkan Energy rata-rata sisanya(AVG_Energy). Dan akan diambil yang rata-rata sisanya paling tinggi.

# 1.5 Cara Kerja 
Kami Melakukan Modifikasi dengan dengan menambahkan upper dan lower threshold berdasarkan sinyal, dimana hanya rreq yang berada diatas lower treshold dan dibawah upper treshold yang akan di forward packetnya, kemudian menambahkan variabel iEnergy kemdian membandingkanya dengan energy dari RREQ lain.

# 2. Implementasi

## 2.1 Deskripsi Sistem
Implementasi routing protocol AODV dilakukan pada sistem dengan :<br/>

Sistem Operasi : Linux Ubuntu 16.04<br/>
Aplikasi yang digunakan :<br/>
NS-2.35 (Instalasi)<br/>

Bahasa C++ dengan memodifikasi algoritma AODV yang sudah ada<br/>
Simulation Environment : TCL script<br/>
Testing: <br/>

# 3. Referensi

https://www.youtube.com/watch?v=pP6KkkICQMI<br/>
https://www.researchgate.net/publication/331941804_Energy_Aware_Path_Selection_based_Efficient_AODV_for_MANETs

